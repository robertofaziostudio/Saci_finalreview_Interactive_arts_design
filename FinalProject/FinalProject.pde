// opencv libs
import gab.opencv.*;
import processing.video.*;
import java.awt.*;
import controlP5.*;

OpenCV opencv;
Capture cam;

VirtualGrid grid;

ArrayList<Ball> balls;
boolean bDrawFlow = false;
float threshold = 1.0f;
int xRes = 3;
int yRes = 3;

void setup() 
{
  size(1280, 800);
  cam = new Capture(this, 640, 480);
  opencv = new OpenCV(this, 640, 480);
  cam.start(); 
  
  grid = new VirtualGrid();
  balls = new ArrayList<Ball>();  
}

void draw() 
{
  background(0);
  // convert to GRAYSCALE
  cam.filter(GRAY);
  
  pushMatrix();
  image(cam, 0, 0);
  opencv.loadImage(cam);
  popMatrix();
  opencv.calculateOpticalFlow();
  
  // DRAW OPTICAL FLOW
  pushMatrix();
  pushStyle();
  //translate(0, cam.height);
  stroke(255);
  if(bDrawFlow)
    opencv.drawOpticalFlow();
  popStyle();
  popMatrix();

  grid.drawGrid();
  grid.draw();
 
  for(int i = 0; i < cam.width; i+=xRes)
  {
    for(int j = 0; j < cam.height; j+=yRes)
    {
      if(opencv.getFlowAt(i,j).x > threshold)
      {
         //Ball b = balls.get(i);
         Ball b = new Ball(new PVector(i,j));
         b.setColor(grid.selectedColor);
         balls.add(b);
      }
     }
   }
  
  for(int i = 0; i < balls.size(); i++)
   {
     Ball tempBall = balls.get(i);
     tempBall.update();
     tempBall.draw();
    }

}

void captureEvent(Capture c) 
{
  c.read();
}

void keyPressed()
{
  if(key =='f')
  {
    bDrawFlow = !bDrawFlow;
  }
  
  if (key == CODED) 
  {
    if (keyCode == UP)
    {
      threshold += 0.2f; 
    }
     if (keyCode == DOWN)
    {
      threshold -= 0.2f; 
    }
    
    println("threshold :", threshold);
  }
  
  
}
