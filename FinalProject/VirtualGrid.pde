
class VirtualGrid
{
  int gridResX = 1;
  int gridResY = 4;
  int gap = 120;
  int size = 50;
  color[] arrayColors = { color(139, 223, 117),
                          color(255, 255, 255),
                          color(242, 237, 89),
                          color(185, 68, 147),
                          color(23,99,44),
                          color(12,88,99),
                          color(110,44,44)
                                            };
                                            
  color selectedColor = color(255,255,255);
  PVector[] flows;
  
  VirtualGrid()
  {
    flows = new PVector[gridResY];
    for(int i = 0; i < flows.length; i++)
    {
      flows[i] = new PVector(0,0,0);
    }
    
    println(flows.length);
  }
  
  void drawGrid()
  {
    pushStyle();
    fill(255);
    noStroke();
    noFill();
    for(int y = 0; y < gridResY; y++)
    {
      for(int x = 0; x < gridResX; x++)
      {
        fill(arrayColors[y]);
        rect(x * gap, y * gap, size, size);
     
        flows[y].set(opencv.getAverageFlowInRegion(x*gap, y*gap ,size,size));
        
        if(abs(flows[y].x) > 2 && abs(flows[y].y) > 2)
        {
          selectedColor = arrayColors[y];
        }
      }
    }
    popStyle();
    

  }
  
  void draw()
  {
    pushStyle();
    fill(selectedColor);
    rect(width - 100, 0, 100, 100);
    popStyle();
  }
  
}
