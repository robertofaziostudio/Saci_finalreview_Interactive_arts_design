//main class that draws a ball, change color, add soem physics, etc
//OOP oriented object progrmaing
class Ball{
 //data
 float x;
 float y;
 float velX;
 float velY;
 float size;
 float alpha;
 color col;
 
 //constructor (says what happenes when I intilize)
 Ball(PVector pos)
 {
   x = pos.x;
   y = pos.y;
   velX = random(-5, 5);
   velY = random(-5, 5);
   size = random(1, 4);
   alpha = 255;
   //this changes color of balls without falshing
   //col = color(random(0, 255),random(0, 255), random(0,255));
   //col = color(255);
 }
 
 
 void update(){
   x += velX;
   y += velY;
   alpha -= 10;
   //-- is the same as -= 1
   // this makes it fade as you drag
   
   //can use this to create gravity, here its like it moves through dish soap
   //using the velY += 0.9 adds gravity
   //velX *= 0.9;
   velY += 0.9;
 }
 
 

 //define methods or functions
  void draw()
  {
    fill (col, alpha);
    noStroke();
    ellipse(x,y, size, size); 
    
  }
  
  void setColor(color c)
  {
    col = c;
  }
  
  
  
}
